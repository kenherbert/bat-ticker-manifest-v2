/*global formatValue, getCurrency */
describe('formatValue function', () => {
    it('returns a string representation of a number to 2 decimal places if `shorten` is not `true`', () => {
        expect(formatValue(0)).toBe('0.00');
        expect(formatValue(-5)).toBe('-5.00');
        expect(formatValue(1.1)).toBe('1.10');
        expect(formatValue(-2.5)).toBe('-2.50');
        expect(formatValue(312.01)).toBe('312.01');
        expect(formatValue(-102.78)).toBe('-102.78');
        expect(formatValue(1.23653)).toBe('1.24');
        expect(formatValue(1.23153)).toBe('1.23');
    });


    it('returns a string representation of a number to minimal lengths depending on number passed', () => {
        expect(formatValue(0, true)).toBe('0.00');
        expect(formatValue(-5, true)).toBe('-5.00');
        expect(formatValue(1.1, true)).toBe('1.10');
        expect(formatValue(12.554656546, true)).toBe('12.6');
        expect(formatValue(312.01, true)).toBe('312');
        expect(formatValue(102.78, true)).toBe('103');
        expect(formatValue(1256, true)).toBe('1.3k');
        expect(formatValue(1231, true)).toBe('1.2k');
        expect(formatValue(10256, true)).toBe('10.3k');
        expect(formatValue(15631, true)).toBe('15.6k');
        expect(formatValue(22256, true)).toBe('22.3k');
        expect(formatValue(15631, true)).toBe('15.6k');
        expect(formatValue(3245647, true)).toBe('3246k');
        //        expect(formatValue(, true)).toBe('15.6k');
    });


    it('returns `0.00` if a non-numeric value is passed', () => {
        expect(formatValue('hi')).toBe('0.00');
        expect(formatValue('6.65786578')).toBe('0.00');
        expect(formatValue(null)).toBe('0.00');
        expect(formatValue(false)).toBe('0.00');
    });
});

describe('getCurrency function', () => {
    it('returns a valid currency when a real currency id is passed', () => {
        expect(getCurrency('usd')).toEqual({
            name: 'US dollars',
            symbol: '$',
            localFormat: '${}'
        });
        expect(getCurrency('try')).toEqual({
            name: 'Turkish lira',
            symbol: '₺',
            localFormat: '₺{}'
        });
    });

    it('returns `false` when an invalid currency id is passed', () => {
        expect(getCurrency('')).toBe(false);
        expect(getCurrency('blah')).toBe(false);
        expect(getCurrency('egp')).toBe(false);
    });
});