/*global chrome, init, getAPIData, getSupportedCurrencies */

chrome.runtime.onStartup.addListener(() => {
    init();
});

chrome.runtime.onInstalled.addListener(() => {
    init();
});

chrome.alarms.onAlarm.addListener(alarm => {
    if(alarm.name === 'BATTickerTimer') {
        getAPIData();
    } else if(alarm.name === 'CurrencyUpdateTimer') {
        getSupportedCurrencies();
    }
});

chrome.runtime.onMessage.addListener(request => {
    if(request.action === 'update settings') {
        getAPIData();
    }

    return true;
});
