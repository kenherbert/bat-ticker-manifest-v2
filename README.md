A Chrome extension to show the current price and 24h change in value for the cryptocurrency BAT against 40+ fiat currencies.

Data is sourced from [CoinGecko's data API](https://www.coingecko.com/en/api), and is updated every minute.

Official release can be found [on the Chrome Web Store](https://chrome.google.com/webstore/detail/fpgnkcakakiminoablpndllfbhobglid).

This repo is for the manifest v2 version of the extension, made obsolete in January 2023. The main repo for the version that will be updated to manifest v3 ca be found [here](https://gitlab.com/kenherbert/bat-ticker).
